# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 12:14:39 2018

@author: JORGE.PORRAS
"""

import datetime
import logging
import os

loggers = {}

class TesterConfig:
    
    global loggers
    
    # Carpeta raíz donde se alamenarán los resultados
        
    RESULT_ROOT = 'resultados'
       
    RESULT_FOLDER_PATH = RESULT_ROOT + '/{0}/'
    
    PREFIX_LIST = ['app10', 'app11']
    #PREFIX_LIST = ['bo', 'core', 'web', 'app10', 'app11']
    
    DATA_BASE_NAME = RESULT_FOLDER_PATH + 'dineromail_{0}.sqlite'
    
    EXCEL_RESULT_FILE_CONNECTIVITY = RESULT_FOLDER_PATH + 'output_connectivity_{0}.xlsx'
    
    def __init__(self, file):
        self.file = os.path.basename(file)
        
        # Obtiene el actual log
    def get_log(self):

        if loggers.get(self.file):
            logger = loggers.get(self.file)
            return logger
        else:
            logger = logging.getLogger(self.file)
            logger.setLevel(logging.INFO)
            now = datetime.datetime.now()
            handler = logging.FileHandler(self.RESULT_ROOT + 
                                          os.path.basename(self.file) + '_' + 
                                          now.strftime("%Y-%m-%d") + 
                                          '.log')

            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

            handler.setFormatter(formatter)
            logger.addHandler(handler)
            loggers[self.file] = logger
            logger.info('Log creado')
            return logger
    