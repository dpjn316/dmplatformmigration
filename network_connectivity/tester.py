# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 17:48:30 2018

@author: JORGE.PORRAS
"""

import pyodbc
import socket
import sqlite3
import urllib.request, urllib.parse, urllib.error
import datetime
import ssl
import os
from shutil import copyfile
from tester_config import TesterConfig as config
import argparse

class EndPointTester:

    SUCCESS = 0
    FAIL = 1

    def testDBConnection(self, conn_str):
        try:
            cnxn_data = pyodbc.connect(conn_str)
            cursor_data = cnxn_data.cursor()
            result = cursor_data.execute('Select @@version').fetchone()
            cnxn_data.close()
            self.database_result = (self.SUCCESS, 'SUCCESS {0}'.format(result))
            return self.database_result
        except Exception as exc:
            self.database_result = (self.FAIL, 'FAIL {0}'.format(exc))
            return self.database_result

    def isDBConnectionOk(self):
        return True if self.database_result[0] == self.SUCCESS else False

    def testTCP(self, host, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((host, port))
            s.shutdown(2)
            print("TCP: Success connecting to {0} on port {1}".format(host, str(port)))
        except:
            print("TCP: Cannot connect to {0} on port {1}".format(host, str(port)))

    def testUDP(self, host, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect((host, port))
            s.shutdown(2)
            print("UDP: Success connecting to {0} on port {1}".format(host, str(port)))
        except:
            print("UDP: Cannot connect to {0} on port {1}".format(host, str(port)))

    def testApplicationLayer(self, url):

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        
        req = urllib.request.Request(url)
        try:
            a = urllib.request.urlopen(req, context=ctx, timeout=10)
            self.application_result = (self.SUCCESS, 'SUCCESS {0} {1}'.format(a.code, a.reason))
            return self.application_result
        except urllib.error.HTTPError as e:
            self.application_result = (self.FAIL, 'FAIL {0} {1}'.format(e.code, e.reason))
            return self.application_result
        except Exception as exc:
            self.application_result = (self.FAIL, 'FAIL {0}'.format(exc))
            return self.application_result

    def isApplicationLayerOk(self):
        return True if self.application_result[0] == self.SUCCESS else False


class Tester:

    PROCESS = 0
    IGNORE = 1 << 0

    AMAZON = 1 << 3
    RACKSPACE = 1 << 6
    
    AMAZON_SUCCESS = (AMAZON >> 2) | AMAZON
    AMAZON_FAIL = (AMAZON >> 1) | AMAZON
    
    RACKSPACE_SUCCESS = (RACKSPACE >> 2) | RACKSPACE
    RACKSPACE_FAIL = (RACKSPACE >> 1) | RACKSPACE
    
    CLEAR_AMAZON_RESULT = IGNORE | AMAZON | RACKSPACE | RACKSPACE_SUCCESS | RACKSPACE_FAIL | PROCESS
    CLEAR_RACKSPACE_RESULT = IGNORE | AMAZON | RACKSPACE | AMAZON_SUCCESS | AMAZON_FAIL | PROCESS

    def __init__(self, cfg, en):
        self.cfg = cfg
        self.ENV = en

        if self.ENV == self.AMAZON:
            self.environment = 'amazon'
            self.CLEAR_PREVIOUS_RESULT = self.CLEAR_AMAZON_RESULT

        if self.ENV == self.RACKSPACE:
            self.environment = 'rackspace'
            self.CLEAR_PREVIOUS_RESULT = self.CLEAR_RACKSPACE_RESULT

        self.SUCCESS = (self.ENV >> 2) | self.ENV
        self.FAIL = (self.ENV >> 1) | self.ENV

    def loadResources(self):

        conn = sqlite3.connect(self.cfg)
        cur = conn.cursor()
        data = cur.execute('''
                           SELECT id, destino_limpio, type, tested
                           FROM DESTINO
                           WHERE type
                           in ("urls", "connectionStrings", "connectionStrings1", "connectionStrings2")
                           AND tested is not null AND tested <> 1
                           AND ignore = 0''').fetchall()
        # tested <> 1 equivale a IGNORE = 1 << 0

        self.resources = [d for d in data]
        conn.close()

    def getConnectionStrings(self):
        return [d for d in self.resources if d[2] in ["connectionStrings", "connectionStrings1", "connectionStrings2"]]

    def getUrls(self):
        return [d for d in self.resources if d[2] == "urls"]

    def testResources(self, elements, testerFunction, isOkTest):

        conn = sqlite3.connect(self.cfg)
        cur = conn.cursor()

        nelementos = len(elements)
        for index,  d in enumerate(elements):
            
            result = testerFunction(d[1])
            
            if isOkTest():
                tested = self.SUCCESS | (int(d[3]) & self.CLEAR_PREVIOUS_RESULT)
            else:
                tested = self.FAIL | (int(d[3]) & self.CLEAR_PREVIOUS_RESULT)

            cur.execute('UPDATE DESTINO SET tested = ?, {0} = ?, fecha_test_{0} = ? WHERE Id = ?'.format(self.environment),
                        (tested, result[1], datetime.datetime.now(), int(d[0])))
            conn.commit()
            print('{0}/{1} {2} <- {3}'.format(index, nelementos, result, d[1]))
        conn.close()
        


        

#parser = argparse.ArgumentParser()
#parser.add_argument("environment", type=str, choices=['AMAZON', 'RACKSPACE'],
#                    help="Ambiente para ejecutar prueba")
#parser.add_argument("-t", "--test", help="Ejecuta pruebas",
#                    action="store_true")
#parser.add_argument("resources", type=str, choices=['all', 'databases', 'url'],
#                    help="Recursos para ejecutar pruebas", default=0)
#
#args = parser.parse_args()
#answer = args.environment
#res = args.resources

cfg = config(__file__)

if not os.path.exists(cfg.RESULT_ROOT):
    os.makedirs(cfg.RESULT_ROOT)
        
    for p in cfg.PREFIX_LIST:
        if not os.path.exists(cfg.RESULT_FOLDER_PATH.format(p)):
            os.makedirs(cfg.RESULT_FOLDER_PATH.format(p))
            copyfile('../configuration_scraping/{1}'.format(p, cfg.DATA_BASE_NAME.format(p)), cfg.DATA_BASE_NAME.format(p)) 


#if args.test:
    
    
for p in cfg.PREFIX_LIST:
        
    database = cfg.DATA_BASE_NAME.format(p)
    cfg.get_log().info(database)
    tester = Tester(database, Tester.AMAZON)
    tester.loadResources()
    ept = EndPointTester()
    #tester.testResources(tester.getUrls(), ept.testApplicationLayer, ept.isApplicationLayerOk)
    
    tester.testResources(tester.getConnectionStrings(), ept.testDBConnection, ept.isDBConnectionOk)





    








#conn_str = 'Driver={SQL Server Native Client 11.0};SERVER=10.24.49.215,6000;DATABASE=PAYUMIGRATION;UID=admin;PWD=%Vanderditus$200'
#url = 'https://docs.python.org/2/howto/urllib2.html'
#
#endpointtester = EndPointTester()
#result = endpointtester.testDBConnection(conn_str)
#print(result)
#result1 = endpointtester.testApplicationLayer(url)
#print(result1)
#
#
#host = "10.24.49.215"
#port = 6000
#testTCP(host, port)
#testUDP(host, port)