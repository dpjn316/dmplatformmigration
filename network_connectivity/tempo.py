# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 16:00:00 2018

@author: JORGE.PORRAS


"""

import pyodbc
import psycopg2

# Trae las transacciones del día de hoy con estado 3 (Notificado por PayU)
def trx_from_dm():
    
    conn = pyodbc.connect("Driver={SQL Server};Server=10.24.51.110;Database=payumigration;Trusted_Connection=yes;")
    
    dm_today_notified_trx_sql = '''
    SELECT LOWER(PayUTransactionId), TransactionAmmount, TransactionFee, MerchantId, MerchantEmail, PaymentDate, Barcode, Provider 
    FROM PAYUMIGRATION..PAYUTRANSACTION
    WHERE CAST(WriteDate AS DATE) >= CAST(GETDATE() AS DATE) AND PayUTransactionStatus = 3
    '''
    
    cursor_data = conn.cursor()
    result = [r for r in cursor_data.execute(dm_today_notified_trx_sql).fetchall()]
    conn.close()
    return result

# Trae 
def trx_from_payu(voucher_uuids):
    
    conn = psycopg2.connect("dbname='pps' user='tester' host='localhost' port=5432 password='test_password'")
    cursor_data = conn.cursor()

    result = []
    for trx in voucher_uuids:
        cursor_data.execute("""
                        select count(1) from transaccion_datos_extra 
                        where dm_tx_coupon_uuid = %(voucher_id)s and fecha_creacion > date_trunc('day', now())
                        """, {'voucher_id': str(trx[0])})
        data = cursor_data.fetchone()        
        if data[0] == 0: result.append(trx)
    
    conn.close()
    return result

print(trx_from_payu(trx_from_dm()))



	
	
	