# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 19:46:52 2018

PASO 2

Dada el archivo (files_to_process.txt) obtenido del PASO 1 que contiene
archivos de los cuales se tiene que extraer la información usando expresiones regulares
y posteriormente almacenarla en la basse de datos
(DATA_BASE_NAME = RESULT_FOLDER_PATH + 'dineromail_' + PREFIX + '.sqlite') definida en el archivo
parameters.py

"""

import re
import sqlite3
from datetime import date, datetime
from parameters import Configuration as config
from scraper_utils import Utils

class ExtractData:

    # Incia el objeto con la configuración del archivo parameters.py
    def __init__(self, cfg):
        self.cfg = cfg

        self.cfg.get_log().info('Iniciando')
        self.cfg.log_config_parameters()
        self.cfg.log_regular_expressions()

        self.file_to_process = self.cfg.RESULT_FOLDER_PATH + self.cfg.FILES_TO_PROCESS

        self.cfg.get_log().info('Checksum archivo para procesar. Md5:{0} - Sha1:{1} -> {2}'.format(
                    Utils.md5(self.file_to_process),
                    Utils.sha1(self.file_to_process),
                    self.file_to_process))

        self.cfg.get_log().info('Borrar base de datos {0}'.format(self.cfg.DELETE_DATABASE))

    # Crear una base de datos para almacenar los datos encontrados
    def create_data_base(self):

        self.conn = sqlite3.connect(cfg.DATA_BASE_NAME)
        self.cur = self.conn.cursor()

        if self.cfg.DELETE_DATABASE:
            self.cur.executescript('''
            DROP TABLE IF EXISTS Destino;
            DROP TABLE IF EXISTS Archivo;
            DROP TABLE IF EXISTS Dominio;
            DROP TABLE IF EXISTS ArchivoProcesado;

            CREATE TABLE Dominio (
                id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                name    TEXT,
                folder  TEXT,
                real_domain TEXT
            );

            CREATE TABLE Archivo (
                id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                name    TEXT,
                path    TEXT,
                dominio_id INTEGER,
                ignore  INT,
                md5     TEXT,
                sha1    TEXT
            );

            CREATE TABLE Destino (
                id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                type  TEXT,
                destino   TEXT,
                contexto  TEXT,
                archivo_id INTEGER,
                ignore  INT
            );

            CREATE TABLE ArchivoProcesado (
                id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                name    TEXT,
                folder  TEXT,
                created DATETIME,
                error TEXT,
                md5     TEXT,
                sha1    TEX
            );

            ''')

    # Guarda los datos encontrados en la base de datos
    def save_to_db(self, domainName, elements):

        self.cur.execute('''INSERT OR IGNORE INTO Dominio (name, folder, real_domain)
            VALUES ( ?, ?, '')''',
                         ( domainName.strip(), self.cfg.ROOT_PATH.strip()) )
        dominio_id = self.cur.lastrowid
        for fi in elements:

            self.cur.execute('''INSERT OR IGNORE INTO Archivo (name, path, dominio_id, ignore, md5, sha1)
                VALUES ( ?, ?, ?, ?, ?, ? )''',
                             ( fi[0].strip(), fi[1].strip(), dominio_id, 0, Utils.md5(self.cfg.ROOT_PATH.strip() + fi[1].strip()), 
                              Utils.sha1(self.cfg.ROOT_PATH.strip() + fi[1].strip())) )
            archivo_id = self.cur.lastrowid

            for detail in fi[2]:

                self.cur.execute('''INSERT OR IGNORE INTO Destino (type, destino, contexto, archivo_id, ignore)
                    VALUES ( ?, ?, ?, ?, ? )''',
                                 (detail[0].strip(), detail[1].strip(), detail[2].strip()[:100], archivo_id, 0) )

        self.conn.commit()

        rstr = '+ Guardando -> {0}'.format(domainName)
        self.cfg.get_log().info(rstr)
        print(rstr)

    # Extrae información con base en las expresiones regulares definidas en parameters.py
    def extract_data_from_file(self, file):

        fhandler = open(self.cfg.ROOT_PATH + file, 'r')

        result = list()

        for line in fhandler:
            for exp in self.cfg.EXPRESSIONS:
                if re.search(exp[1], line):
                    for destination in re.findall(exp[1], line):
                        result.append((exp[0], destination, line))

        fhandler.close()

        rstr = '- Procesando <- {0} elementos en {1}'.format(len(result), file)
        self.cfg.get_log().info(rstr)
        print(rstr)
        return result

    # Ejecuta el proceso de extracción de datos en los archivos mediante expresiones regulares
    # y su posterior almacenamiento en base de datos
    def process(self):

        try:

            files_to_be_processed_handler = open(self.file_to_process, 'r')

            domains = dict()
            current_domain = None
            domain_to_save = None
            files = list()


            for file_to_be_processed in files_to_be_processed_handler:

                try:

                    self.cur.execute('SELECT count(*) FROM ArchivoProcesado WHERE name = ? and folder = ? ',
                                (file_to_be_processed.strip(), cfg.ROOT_PATH.strip()))
                    must_processed = self.cur.fetchone()[0]

                    if must_processed == 0:

                        if current_domain is None:
                            current_domain = file_to_be_processed[:file_to_be_processed.rindex('\\')]
                            domain_to_save = current_domain
                        else:
                            current_domain = file_to_be_processed[:file_to_be_processed.rindex('\\')]

                            if len(domain_to_save) < len(current_domain):
                                if not current_domain.startswith(domain_to_save):
                                    domains[domain_to_save] = files
                                    self.save_to_db(domain_to_save, domains[domain_to_save])
                                    domain_to_save = current_domain
                                    files = list()
                                    result = list()
                            elif len(domain_to_save) > len(current_domain):
                                if not domain_to_save.startswith(current_domain):
                                    domains[domain_to_save] = files
                                    self.save_to_db(domain_to_save, domains[domain_to_save])
                                    files = list()
                                    result = list()
                                domain_to_save = current_domain
                            else:
                                if not domain_to_save.startswith(current_domain):
                                    domains[domain_to_save] = files
                                    self.save_to_db(domain_to_save, domains[domain_to_save])
                                    domain_to_save = current_domain
                                    files = list()
                                    result = list()

                        result = self.extract_data_from_file(file_to_be_processed.strip())

                        if not (result is None or len(result) == 0):
                            files.append((file_to_be_processed[file_to_be_processed.rindex('\\') + 1:],
                                                               file_to_be_processed, result))

                        self.cur.execute('INSERT OR IGNORE INTO ArchivoProcesado (name, folder, created, md5, sha1) VALUES ( ?, ?, ?, ?, ? )',
                                         (file_to_be_processed.strip(), 
                                          cfg.ROOT_PATH.strip(), 
                                          datetime.today(), 
                                          Utils.md5(self.cfg.ROOT_PATH + file_to_be_processed.strip()), 
                                          Utils.sha1(self.cfg.ROOT_PATH + file_to_be_processed.strip())))

                    else:
                        rstr = 'Archivo ya procesado {0}'.format(file_to_be_processed.strip())
                        cfg.get_log().info(rstr)
                        print(rstr)


                except Exception as err:
                    cfg.get_log().error('{0} {1} {2} {3}'.format(file_to_be_processed.strip(), cfg.ROOT_PATH.strip(), datetime.today(), str(err)))
                    print(err)
                    self.cur.execute('INSERT OR IGNORE INTO ArchivoProcesado (name, folder, created, error, md5, sha1) VALUES ( ?, ?, ?, ?, ?, ? )',
                                     (file_to_be_processed.strip(), 
                                      cfg.ROOT_PATH.strip(), 
                                      datetime.today(), 
                                      str(err), 
                                      Utils.md5(self.cfg.ROOT_PATH + file_to_be_processed.strip()), 
                                      Utils.sha1(self.cfg.ROOT_PATH + file_to_be_processed.strip())))

            if domain_to_save is not None:
                domains[domain_to_save] = files
                self.save_to_db(domain_to_save, domains[domain_to_save])

            files_to_be_processed_handler.close()
            self.conn.close()

            cfg.get_log().info('Finalizado')
        except Exception as exc:
            cfg.get_log().fatal(exc)
            print(exc)

#####################################################
cfg = config(__file__)
ed = ExtractData(cfg)
ed.create_data_base()
ed.process()
