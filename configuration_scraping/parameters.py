# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 16:52:10 2018

@author: JORGE.PORRAS

PASO 0

Contiene los parámetros de configuración para realizar el escaneo de 
archivos de DM

Los parámetros para configurar son:
    
    root_path = Contiene la ruta raíz donde se va a realizar el escaneo  
    folders_to_scan = Carpetas donse donde se va a realizar el escaneo
    prefix = Un identificador separar cada de los recursos compartidos donde se va a realizar el escaneo
    
"""

import datetime
import logging
import os

loggers = {}

class Configuration:

    global loggers

    ################################################################################################
    ####### CONFIGURAR LOS SIGUIENTES PARAMETROS PARA EL ESCANEO EN CADA RECURSO COMPARTIDO ########
    ################################################################################################


    # Ruta principal para iniciar exploración de configuraciones
    ROOT_PATH = '//10.24.49.215/servicios_app11$/'
      
    # Prefijo para identificar la máquina que se está procesando
    PREFIX = 'app11'
      
    # Carpetas para procesar
    FOLDERS_TO_SCAN = [
      
        'AcreditadorCBsArgentina',
        'AcreditadorCBsArgentinaViejos',
        'AcreditadorCBsMexico',
        'ActiveX_Apps',
        'ActiveX_APPS_WEB',
        'barcode',
        'barcode--bk',
        'BarCodeFileAcreditation.DetailsProcessor',
        'BarCodeFileAcreditation.MailProcessor',
        'BarcodeGeneratorApp',
        'Componentes Compartidos Apps y Web',
        'Copy of barcode',
        'CotizadorDolar',
        'Dialect',
        'DM.SellerTools.EmailSender',
        'DMtiendaWebCapture',
        'DM_Clarin',
        'DM_Integration',
        'DM_Notificador',
        'DM_Notificador2',
        'DM_Notificador3',
        'DM_Notificador4',
        'DM_PrestoCuadratura',
        'DM_RefundBR',
        'DM_RobotEXE',
        'DM_RobotTC',
        'DM_RobotTC1',
        'DownloadersCBsArgentinaViejos',
        'Envio Mails',
        'MailProcessor',
        'PendingNotificationProcessor',
        'PrePaidCardAccreditationLayer',
        'PrePaidCardBarcodeAccreditationApp',
        'RecurringBilling',
        'RetiroFondosAutomaticos',
        'RipleyRendicion',
        'RobotServer',
        'SQLTransferData',
        'SSRSRenamer_Setup_1.0.0',
        'XX-APP'
      
    ]
      
    DELETE_DATABASE = True
    


    ################################################################################################
    #######                                                                                 ########
    ################################################################################################

    # Extensiones que se tendrán en cuenta durante el procesamiento
    EXTENSIONS_TO_PROCESS = ['.txt', '.config', '.xml', '.asp', '.asa', '.cls', '.js', '.cshtml', '.vb', '.inc']

    # Carpeta raíz donde se alamenarán los resultados
    
    RESULT_ROOT = 'resultados'
    
    VISUALIZATION_RESULT_FOLDER = RESULT_ROOT + '/' + 'resultado_visualizacion'
    
    RESULT_FOLDER_PATH = RESULT_ROOT + '/' + PREFIX + '/'
    
    FILES_TO_PROCESS = 'files_to_process.txt'
    
    FILES_TO_SKIP = 'files_to_skip.txt'
    
    DATA_BASE_NAME = RESULT_FOLDER_PATH + 'dineromail_' + PREFIX + '.sqlite'
    
    EXCEL_RESULT_FILE = RESULT_FOLDER_PATH + 'output_' + PREFIX + '.xlsx'
    
    EXCEL_RESULT_FILE_CONNECTIVITY = RESULT_FOLDER_PATH + 'output_connectivity_' + PREFIX + '.xlsx'
    
    # Obtiene un listado de todos los destinos con el fin que se pueda generar un listado de destinos a ignorar
    DESTINATION_LIST = RESULT_FOLDER_PATH + 'destination_' + PREFIX + '.txt'
    
    # listado de destinos a ignorar
    BLACK_LIST = RESULT_ROOT  + '/' + 'black_list.txt'
    

    # Expresiones regulares para identificar para extraer configuración de aplicaciones
    urls_regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    ips_regex = '(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})'
    ftpurls_regex = '[s]*ftp?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    emails_regex = '[a-zA-Z0-9+_\-\.]+@[0-9a-zA-Z][.-0-9a-zA-Z]*.[a-zA-Z]+'
    network_regex = '[a-zA-Z0-9]*[:]*\\\\(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\)/,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    network1_regex = '[a-zA-Z0-9]*[:]*\/[^\\\/\_\*\?\"\<\>\|](?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\)/,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    connectionStrings_regex = 'Data Source=.+'
    connectionStrings1_regex = 'DSN=.+'
    connectionStrings2_regex = 'Server=.+'

    EXPRESSIONS = list()

    EXPRESSIONS.append(('urls', urls_regex));
    EXPRESSIONS.append(('ips', ips_regex));
    EXPRESSIONS.append(('ftpurls', ftpurls_regex));
    EXPRESSIONS.append(('emails', emails_regex));
    EXPRESSIONS.append(('network', network_regex));
    #EXPRESSIONS.append(('network1', network1_regex));
    EXPRESSIONS.append(('connectionStrings', connectionStrings_regex));
    EXPRESSIONS.append(('connectionStrings1', connectionStrings1_regex));
    EXPRESSIONS.append(('connectionStrings2', connectionStrings2_regex));

    # Recibe el nombre del archivo para asociarlo al handler del log
    def __init__(self, file):
        self.file = os.path.basename(file)

    # Obtiene el actual log
    def get_log(self):

        if loggers.get(self.file):
            logger = loggers.get(self.file)
            return logger
        else:
            logger = logging.getLogger(self.file)
            logger.setLevel(logging.INFO)
            now = datetime.datetime.now()
            handler = logging.FileHandler(self.RESULT_FOLDER_PATH + 
                                          os.path.basename(self.file) + '_' + 
                                          now.strftime("%Y-%m-%d") + 
                                          '.log')

            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

            handler.setFormatter(formatter)
            logger.addHandler(handler)
            loggers[self.file] = logger
            logger.info('Log creado')
            return logger

    # Escribe en log los parámetros de configuración para realizar el escaneo
    def log_config_parameters(self):
        self.get_log().info('Configuración: Ruta raíz para escanear {0}'.format(self.ROOT_PATH))
        self.get_log().info('Configuración: Prefijo {0}'.format(self.PREFIX))
        self.get_log().info('Configuración: Extensiones para procesar {0}'.format(self.EXTENSIONS_TO_PROCESS))
        self.get_log().info('Configuración: Carpetas para escanear {0}'.format(self.FOLDERS_TO_SCAN))
        self.get_log().info('Configuración: Carpeta de resultados {0}'.format(self.RESULT_FOLDER_PATH))
        self.get_log().info('Configuración: Base de datos {0}'.format(self.DATA_BASE_NAME))
        self.get_log().info('Configuración: Archivo de Excel {0}'.format(self.EXCEL_RESULT_FILE))
        self.get_log().info('Configuración: Archivo de destinos para depurar {0}'.format(self.DESTINATION_LIST))
        self.get_log().info('Configuración: Lista negra {0}'.format(self.BLACK_LIST))
        

    # Escribe en log las expresiones regulares para realizar la extracción de datos
    def log_regular_expressions(self):
        self.get_log().info('Configuración: Expresiones regulares {0}'.format(self.EXPRESSIONS))
