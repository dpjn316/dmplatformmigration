# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 18:33:32 2018

@author: JORGE.PORRAS

PASO 1

Dada una ruta raíz, un listado de carpetas y un listado de extensiones de archivo, se
procede a generar dos archivos que contienen los archivos que van a ser procesados
posteriormente y otro que contiene los archivos que van a ser excluidos.

"""

import glob
import os
from parameters import Configuration as config
from scraper_utils import Utils

try:
    cfg = config(__file__)
    
    if not os.path.exists(cfg.RESULT_FOLDER_PATH):
        os.makedirs(cfg.RESULT_FOLDER_PATH)
  
    cfg.get_log().info('Iniciando')
    cfg.log_config_parameters()
    cfg.log_regular_expressions()
    
    
    file_to_process_name = cfg.RESULT_FOLDER_PATH + cfg.FILES_TO_PROCESS
    file_to_skip_name = cfg.RESULT_FOLDER_PATH + cfg.FILES_TO_SKIP
    
    fhandler_files_to_process = open(file_to_process_name, 'w')
    fhandler_files_to_skip = open(file_to_skip_name, 'w')
    
    
    n = 0
    while n < len(cfg.FOLDERS_TO_SCAN):
        folder = cfg.FOLDERS_TO_SCAN[n]

        try:
            for name in glob.iglob(cfg.ROOT_PATH + folder + '/**', recursive=True):             
                print('Procesando', name.replace(cfg.ROOT_PATH, ''))
                try:
                    if any(x in name for x in cfg.EXTENSIONS_TO_PROCESS):
                        cfg.get_log().info('Procesando {0} -> aceptado'.format(name))
                        fhandler_files_to_process.writelines(name.replace(cfg.ROOT_PATH, '') + '\n')
                    else:                
                        cfg.get_log().info('Procesando {0} -> rechazado'.format(name))
                        fhandler_files_to_skip.writelines(name.replace(cfg.ROOT_PATH, '') + '\n')        
                except Exception as exc:
                    print(exc)
                    cfg.get_log().error(exc)
            n += 1 
        except StopIteration as exc:
            print(exc)
            
    
    fhandler_files_to_process.close()
    fhandler_files_to_skip.close()
    
    cfg.get_log().info('Checksum archivo. Md5:{0} - Sha1:{1} -> {2}'.format(
            Utils.md5(file_to_process_name), 
            Utils.sha1(file_to_process_name),
            file_to_process_name))

    cfg.get_log().info('Checksum archivo. Md5:{0} - Sha1:{1} -> {2}'.format(
            Utils.md5(file_to_skip_name), 
            Utils.sha1(file_to_skip_name),
            file_to_skip_name))
    
    cfg.get_log().info('Finalizado')
except Exception as exc:
    cfg.get_log().error(exc)
    print(exc)