# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 21:42:02 2018

@author: JORGE.PORRAS

PASO (Intermedio) 2.6 

Dado un archivo de destinos inválidos, se hace actualización en la base 
de datos para ignorarlos

"""

import sqlite3
from parameters import Configuration as config

try:
    cfg = config(__file__)
    
    cfg.get_log().info('Iniciando')
    cfg.log_config_parameters()   

    conn = sqlite3.connect(cfg.DATA_BASE_NAME)
    cur = conn.cursor()
    
    fhandler = open(cfg.BLACK_LIST, 'r')
    
    for line in fhandler:
        cur.execute(''' UPDATE DESTINO SET ignore = 1 WHERE Destino = ?''', (line.strip(),))
    
    fhandler.close()
    
    conn.commit()
    conn.close()
    
    cfg.get_log().info('Finalizado')

except Exception as exc:
    cfg.get_log().error(exc)
    print(exc)       