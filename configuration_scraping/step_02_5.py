# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 21:35:43 2018

@author: JORGE.PORRAS

PASO (Intermedio) 2.5 

Obtiene un listado de todos los destinos con el fin que se pueda
generar un listado de destinos a ignorar

"""

import sqlite3
from parameters import Configuration as config

try:
    cfg = config(__file__)
    
    cfg.get_log().info('Iniciando')
    cfg.log_config_parameters()    
    
    conn = sqlite3.connect(cfg.DATA_BASE_NAME)
    cur = conn.cursor()
    
    cur.execute('''
        SELECT DISTINCT Destino
        FROM DESTINO
        order by 1 desc            
    ''')
    data = cur.fetchall()
    
    fhandler = open(cfg.DESTINATION_LIST, 'w')
    
    for d in data:
        fhandler.write(d[0] + '\n')
    
    fhandler.close()
    conn.close()
    
    cfg.get_log().info('Finalizado')
    
except Exception as exc:
    cfg.get_log().error(exc)
    print(exc)      