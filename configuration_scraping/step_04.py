# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 21:53:07 2018

@author: JORGE.PORRAS

PASO 4

Dado un archivo de Excel usado para una revisión manual, se carga para 
actualizar destinos inválidos

"""

import pandas as pd
from openpyxl import load_workbook
import sqlite3
from parameters import Configuration as config

try:
    cfg = config(__file__)
    
    cfg.get_log().info('Iniciando')
    cfg.log_config_parameters()   
    
    
    conn = sqlite3.connect(cfg.DATA_BASE_NAME)
    cur = conn.cursor()
    
    wb = load_workbook(cfg.EXCEL_RESULT_FILE)
    sheet = wb.get_sheet_by_name('Sheet1')
    
    df = pd.DataFrame(sheet.values)
    
    ignored = df[df[12] == 1] # filtrar las ignaradas 
    
    for a in ignored[8]:
        cur.execute('''UPDATE Destino SET ignore = 1 WHERE id = ? ''', (a,) )
        
    conn.commit()
    
    print(len(ignored[8]))
    
    wb.close()
    conn.close()
    
    cfg.get_log().info('Finalizado')
    
except Exception as exc:
    cfg.get_log().error(exc)
    print(exc)       