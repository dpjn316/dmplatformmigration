# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 19:53:50 2018

@author: JORGE.PORRAS
"""

import hashlib
BLOCKSIZE = 65536

# Librería de utilidades
class Utils:

    # Calcula el md5 de un archivo
    def md5(file):
        hasher = hashlib.md5()
        with open(file, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
        return hasher.hexdigest()

    # Calcula sha-1 de un archivo
    def sha1(file):
        hasher = hashlib.sha1()
        with open(file, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
        return hasher.hexdigest()