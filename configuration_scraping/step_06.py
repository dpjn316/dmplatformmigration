# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 11:32:02 2018

@author: JORGE.PORRAS

Limpieza de urls y de cadenas de conexión

"""

import re
import sqlite3
from parameters import Configuration as config
cfg = config(__file__)

def limpiaUrls():
    urls_regex = '.*(^[^\?\'\<\>\)]+)'
    try:
        cfg = config(__file__)
        
        cfg.get_log().info('Iniciando')
        cfg.log_config_parameters()   
    
        conn = sqlite3.connect(cfg.DATA_BASE_NAME)
        cur = conn.cursor()
        
       
        cur.execute('SELECT Id, Destino FROM DESTINO WHERE TYPE = "urls"')
        data = cur.fetchall()
        
        
        for d in data:
            result = d[1]
            if re.search(urls_regex, result):
                result = re.findall(urls_regex, result)[0]            
                cfg.get_log().info('Actualizado Id={0} -> {1}'.format(int(d[0]), result))
                
            cur.execute('UPDATE DESTINO SET destino_limpio = ?, tested = 0 WHERE Id = ?', (result, int(d[0])))
        
        conn.commit()
        conn.close()
        
        cfg.get_log().info('Finalizado')
    
    except Exception as exc:
        cfg.get_log().error(exc)
        print(exc) 

def limpiaConnStr1():
    ds_regex = 'data[ ]*source[ ]*=[ ]*(.*?);'
    db_regex = 'initial[ ]*catalog[ ]*=[ ]*(.*?);'
    user_regex = 'user[ ]*[id]*[ ]*=[ ]*(.*?);'
    pass_regex = 'password[ ]*=[ ]*(.*?)[";]'
    pass1_regex = 'password[ ]*=[ ]*(.*?);'
    pass2_regex = 'password[ ]*=[ ]*(.*?)"'
    
    try:
        cfg = config(__file__)
        
        cfg.get_log().info('Iniciando')
        cfg.log_config_parameters()   
    
        conn = sqlite3.connect(cfg.DATA_BASE_NAME)
        cur = conn.cursor()
        
       
        cur.execute('SELECT Id, Destino FROM DESTINO WHERE TYPE = "connectionStrings"')
        data = cur.fetchall()
        
        
        for d in data:
            result = d[1]
            
            if (re.search(ds_regex, result, re.IGNORECASE) and 
                re.search(db_regex, result, re.IGNORECASE) and
                re.search(user_regex, result, re.IGNORECASE) and
                re.search(pass_regex, result, re.IGNORECASE)
                ):
                
#                if re.search(pass1_regex, result, re.IGNORECASE):
#                    password = re.findall(pass1_regex, result, re.IGNORECASE)[0]
#                elif re.search(pass2_regex, result, re.IGNORECASE): 
#                    password = re.findall(pass2_regex, result, re.IGNORECASE)[0]
#                else:
#                    password = 'INVALID PASSWORD'
                    
                server = re.findall(ds_regex, result, re.IGNORECASE)[0]
                database = re.findall(db_regex, result, re.IGNORECASE)[0]
                user = re.findall(user_regex, result, re.IGNORECASE)[0]
                password = re.findall(pass_regex, result, re.IGNORECASE)[0]
                                
                result = 'DRIVER={4};SERVER={0};DATABASE={1};UID={2};PWD={3}'.format(server, database, user, password, '{SQL Server Native Client 11.0}')
                  
            else:
                result = 'NO MATCH ' + result
                
            cur.execute('UPDATE DESTINO SET destino_limpio = ?, tested = 0 WHERE Id = ?', (result, int(d[0])))
        
        conn.commit()
        conn.close()
        
        cfg.get_log().info('Finalizado')
    
    except Exception as exc:
        cfg.get_log().error(exc)
        print(exc) 
        
def limpiaConnStr2():
    ds_regex = 'dsn[ ]*=[ ]*(.*?);'
    user_regex = 'uid[ ]*[id]*[ ]*=[ ]*(.*?);'
    pass_regex = 'pwd[ ]*=[ ]*(.*?)[";]'
    
    try:
        cfg = config(__file__)
        
        cfg.get_log().info('Iniciando')
        cfg.log_config_parameters()   
    
        conn = sqlite3.connect(cfg.DATA_BASE_NAME)
        cur = conn.cursor()
        
       
        cur.execute('SELECT Id, Destino FROM DESTINO WHERE TYPE = "connectionStrings1"')
        data = cur.fetchall()
        
        
        for d in data:
            result = d[1]
            
            if (re.search(ds_regex, result, re.IGNORECASE) and 
                re.search(user_regex, result, re.IGNORECASE) and
                re.search(pass_regex, result, re.IGNORECASE)
                ):
                                    
                server = re.findall(ds_regex, result, re.IGNORECASE)[0]
                user = re.findall(user_regex, result, re.IGNORECASE)[0]
                password = re.findall(pass_regex, result, re.IGNORECASE)[0]
                                
                result = 'DSN={0};UID={1};PWD={2}'.format(server, user, password)
                
            else:
                result = 'NO MATCH ' + result                
                
            cur.execute('UPDATE DESTINO SET destino_limpio = ?, tested = 0 WHERE Id = ?', (result, int(d[0])))
        
        conn.commit()

        conn.close()
        
        cfg.get_log().info('Finalizado')
    
    except Exception as exc:
        cfg.get_log().error(exc)
        print(exc) 

def limpiaConnStr3():
    ds_regex = 'server[ ]*=[ ]*(.*?);'
    db_regex = 'database[ ]*=[ ]*(.*?)[";]'
    user_regex = 'user[ ]*[id]*[ ]*=[ ]*(.*?);'
    pass_regex = 'password[ ]*=[ ]*(.*?)[";]'
    
    try:
        cfg = config(__file__)
        
        cfg.get_log().info('Iniciando')
        cfg.log_config_parameters()   
    
        conn = sqlite3.connect(cfg.DATA_BASE_NAME)
        cur = conn.cursor()
        
       
        cur.execute('SELECT Id, Destino FROM DESTINO WHERE TYPE = "connectionStrings2"')
        data = cur.fetchall()
        
        
        for d in data:
            result = d[1]
            
            if (re.search(ds_regex, result, re.IGNORECASE) and 
                re.search(db_regex, result, re.IGNORECASE) and
                re.search(user_regex, result, re.IGNORECASE) and
                re.search(pass_regex, result, re.IGNORECASE)
                ):
                                    
                server = re.findall(ds_regex, result, re.IGNORECASE)[0]
                database = re.findall(db_regex, result, re.IGNORECASE)[0]
                user = re.findall(user_regex, result, re.IGNORECASE)[0]
                password = re.findall(pass_regex, result, re.IGNORECASE)[0]
                                
                result = 'DRIVER={4};SERVER={0};DATABASE={1};UID={2};PWD={3}'.format(server, database, user, password, '{SQL Server}')
                
            else:
                result = 'NO MATCH ' + result                
                
            cur.execute('UPDATE DESTINO SET destino_limpio = ?, tested = 0 WHERE Id = ?', (result, int(d[0])))
        
        conn.commit()
        conn.close()
        
        cfg.get_log().info('Finalizado')
    
    except Exception as exc:
        cfg.get_log().error(exc)
        print(exc)        
        
limpiaUrls()
limpiaConnStr1()
limpiaConnStr2()
limpiaConnStr3()