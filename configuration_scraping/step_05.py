# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 22:00:22 2018

@author: JORGE.PORRAS

Paso Final

Visualización de datos con DS3.js

"""


import sqlite3
import json
import os
from shutil import copyfile
from shutil import copytree
from parameters import Configuration as config



cfg = config(__file__)

main_folder = cfg.VISUALIZATION_RESULT_FOLDER

folder = cfg.PREFIX + '\\'

if not os.path.exists(main_folder):
    os.makedirs(main_folder)
    
if not os.path.exists(main_folder + '/' + folder):
    os.makedirs(main_folder + '/' + folder)

conn = sqlite3.connect(cfg.DATA_BASE_NAME)
cur = conn.cursor()

cur.execute('SELECT NAME FROM Dominio ORDER BY 1')
domiansData = cur.fetchall()

domains = []
for d in domiansData:
    domains.append(str(d[0]))
    
#domains = []
#domains.append('monitor')
for domain in domains:
    
    #if not os.path.exists(main_folder + '/' + folder + '/' + domain):
    #    os.makedirs(main_folder + '/' + folder + '/' + domain)
    
    domainfolder = domain.replace('\\', '_')
        
    copytree('resources/base_data_visualization/app/', main_folder + '/' + folder + '/' + domainfolder)    
    
    if not os.path.exists(main_folder + '/' + folder + '/' + domainfolder + '/data/default'):
        os.makedirs(main_folder + '/' + folder + '/' + domainfolder + '/data/default')    
    
    cur.execute('''
    SELECT distinct dom.name nombre_dominio, dom.folder, arc.name nombre_archivo, arc.path ruta_archivo, des.type, 
    CASE des.type
    	WHEN "connectionStrings" THEN des.id
    	 ELSE des.destino
    END corto, 
    des.contexto,
    des.id id_destino,
    des.destino largo
    FROM Dominio dom
    JOIN Archivo arc ON dom.id = arc.dominio_id
    JOIN Destino des ON arc.id = des.archivo_id
    WHERE des.ignore = 0 and dom.name = ?
    ORDER BY des.id           
    ''', (domain,))
    data = cur.fetchall()
    
    path_data_config = main_folder + '/' + folder + '/' + domainfolder + '/data/default/'
    
    
    jsondata = []
    elements = 0
    for d in data:
    
        archivo_normalizado = str(d[5])
        for ch in ['\\','/',':','*','?','"','<','>','|']:
            if ch in archivo_normalizado:
                archivo_normalizado=archivo_normalizado.replace(ch,"_")    
        
        jsondata.append(
                {      
                    "type"    : str(d[4]),        
                    "group"   : "domain",        
                    "name"    : archivo_normalizado,
                    "depends" : [str(d[0])]
                    }
                )
              
        try:
            file = open(path_data_config + archivo_normalizado + '.mkdn','w') 
     
            file.write('''
    ************************************************************************************************
    Origen: {0}
    Destitno ({6}): {4}
    ************************************************************************************************
    Dominio: {0}
    Ruta fisica: notepad++ {1}
    Ruta relativa: {2}
    Archivo: {3}
    Linea donde se encuentra (contexto): {5}
    '''.format(str(d[0]), str(d[1]).replace("/","\\")+str(d[3]), str(d[3]), str(d[2]), str(d[8]), str(d[6]), str(d[4]))) 
            file.close() 
        except:
            print('Error generando archivo', file)
            file.close()
        elements += 1;
    jsondata.append(
                {      
                    "type"    : "domain",        
                    "group"   : "domain",        
                    "name"    : domain,
                    "depends" : []
                    }
                )    
                
    
    with open(path_data_config + 'objects.json', 'w') as outfile:  
        json.dump(jsondata, outfile)
    outfile.close()
    
    copyfile('resources/base_data_visualization/config.json', path_data_config + 'config.json')  
    
    print(domain, elements)
conn.close()    