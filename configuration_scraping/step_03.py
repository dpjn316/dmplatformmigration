# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 21:23:08 2018

@author: JORGE.PORRAS

PASO 3

Dada la base de datos generada en el PASO 2 (DATA_BASE_NAME = RESULT_FOLDER_PATH + 'dineromail_' + PREFIX + '.sqlite') 
definida en el archivo parameters.py se procede a generar un archivo en Excel 
(EXCEL_RESULT_FILE = RESULT_FOLDER_PATH + 'output_' + PREFIX + '.xlsx') que contiene el resultado
para ser entregado a Infraestructura

"""

import sqlite3
from xlsxwriter.workbook import Workbook
from parameters import Configuration as config

try:
    cfg = config(__file__)
    
    cfg.get_log().info('Iniciando')
    cfg.log_config_parameters()
        
    conn = sqlite3.connect(cfg.DATA_BASE_NAME)
    cur = conn.cursor()
    
    data = cur.execute('''
        SELECT dom.id id_dominio, dom.name nombre_dominio, dom.folder, dom.real_domain,
        arc.id id_archivo, arc.name nombre_archivo, arc.path ruta_archivo, arc.ignore ignorar_archivo,
        des.id id_destino, des.type, des.destino, des.contexto, des.ignore ignorar_destino
        FROM Dominio dom
        JOIN Archivo arc ON dom.id = arc.dominio_id
        JOIN Destino des ON arc.id = des.archivo_id
        WHERE des.ignore = 0
        ORDER BY id_destino        
    '''
    )
    
    workbook = Workbook(cfg.EXCEL_RESULT_FILE)
    worksheet = workbook.add_worksheet()
    
    for i, row in enumerate(data):
        for j, value in enumerate(row):
            worksheet.write(i, j, value)
    workbook.close()

    conn.close()
    
    cfg.get_log().info('Finalizado')
    
except Exception as exc:
    cfg.get_log().error(exc)
    print(exc)    

# COLUMNAS DEL ARCHIVO DE EXCEL
#id_dominio	nombre_dominio	dom.folder	dom.real_domain	id_archivo	nombre_archivo	ruta_archivo	ignorar_archivo	id_destino	des.type	des.destino	des.contexto	ignorar_destino'