
---

**Actividades asociadas al área de desarrallo de DineroMail en el proceso de migración de proveedor.**
## Contexto
En la plataforma de DM los valores de las configuraciones de las aplicaciones ha estado dispersas a través de varios archivos y registros en base de datos como falta de estrictos lineamentos en su desarrollo.
## Fundamentación
Dado lo anterior, se vio la necesidad de realizar un programa que haga un barrido de los archivos de las aplicaciones de DM que se encuentran en todas las máquinas.
## Objetivo
Extraer información relacionada a urls, ips, ftps, email, conexiones a bases de datos y recursos de red para ser consolidados en un único archivo.

---